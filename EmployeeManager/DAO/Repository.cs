﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmployeeManager.Models;
using System.Data;

namespace EmployeeManager.DAO
{
    public class Repository : IRepository
    {
        private SqlConnection _db;

        public Repository()
        {
            var connectString = new SqlConnectionStringBuilder
            {
                DataSource = @"(LocalDB)\MSSQLLocalDB",
                AttachDBFilename = @"D:\Projects\CSharp\EmployeeManager\EmployeeManager\Employee.mdf",
                InitialCatalog = "Emloyee",
                IntegratedSecurity = true
            }.ConnectionString;

            _db = new SqlConnection(connectString);
        }
        public async Task AddEmplDocPhone(EmplDocPhoneModel model)
        {
            var sqlPars = new
            {
                model.LastName,
                model.FirstName,
                model.SecondName,
                model.IsMale,
                model.BirthDtm,
                model.Serial,
                model.Number,
                TypeId = model.DocTypeId,
                model.DeliveryDtm,
                model.WhoDeliver,
                PhoneType = model.PhoneTypeId,
                model.PhoneNumber
            };

            await _db.ExecuteAsync("dbo.uspAddEmlDocPhone", sqlPars, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteEmplDocPhone(EmplDocPhoneModel model)
        {
            var sqlPars = new
            {
                model.EmplId,
                model.DocId,
                model.PhoneId,
            };

            await _db.ExecuteAsync("dbo.uspDeleteEmplDocPhone", sqlPars, commandType: CommandType.StoredProcedure);
        }

        public async Task<IEnumerable<EmplDocPhoneModel>> GetEmplDocPhone()
        {
            var result =  await _db.QueryAsync<EmplDocPhoneModel>("dbo.uspGetEmplDocPhone", null, commandType: CommandType.StoredProcedure);

            return result;
        }
        public async Task UpdateEmplDocPhone(EmplDocPhoneModel model)
        {
            var sqlPars = new
            {
                model.EmplId,
                model.LastName,
                model.FirstName,
                model.SecondName,
                model.IsMale,
                model.BirthDtm,
                model.DocId,
                model.Serial,
                model.Number,
                TypeId = model.DocTypeId,
                model.DeliveryDtm,
                model.WhoDeliver,
                model.PhoneId,
                PhoneType = model.PhoneTypeId,
                model.PhoneNumber
            };

            await _db.ExecuteAsync("dbo.uspUpdateEmplDocPhone", sqlPars, commandType: CommandType.StoredProcedure);
        }

        public void Dispose()
        {
            _db.Close();
            _db.Dispose();
        }
    }
}
