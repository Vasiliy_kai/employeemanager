﻿using EmployeeManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManager.DAO
{
    public interface IRepository : IDisposable
    {
        Task AddEmplDocPhone(EmplDocPhoneModel model);
        Task DeleteEmplDocPhone(EmplDocPhoneModel model);
        Task UpdateEmplDocPhone(EmplDocPhoneModel model);
        Task<IEnumerable<EmplDocPhoneModel>> GetEmplDocPhone();
    }
}


