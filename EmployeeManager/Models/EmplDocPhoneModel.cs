﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManager.Models
{
    public class EmplDocPhoneModel
    {
        public int EmplId { get; set; }
        public int DocId { get; set; }
        public int PhoneId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public bool IsMale { get; set; }
        public string IsMaleName { get; set; }
        public DateTime BirthDtm { get; set; }
        public int DocTypeId { get; set; }
        public string DocType { get; set; }
        public int Serial { get; set; }
        public int Number { get; set; }
        public DateTime DeliveryDtm { get; set; }
        public string WhoDeliver { get; set; }
        public int PhoneTypeId { get; set; }
        public string PhoneType { get; set; }
        public string PhoneNumber { get; set; }
    }
}
