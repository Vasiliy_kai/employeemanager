﻿using EmployeeManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManager
{
    class Debug
    {
        public void Start()
        {
            var emplDocPhone = new EmplDocPhoneModel()
            {
                LastName = "Fomenko",
                FirstName = "Alexandr",
                SecondName = "Petrovich",
                BirthDtm = new DateTime(1975, 03, 24),
                IsMale = true,
                DocTypeId = 1,
                Serial = 0205,
                Number = 123123,
                DeliveryDtm = new DateTime(2000, 05, 29),
                WhoDeliver = "GUVD",
                PhoneTypeId = 1,
                PhoneNumber = "918555555"
            };
        }
    }
}
