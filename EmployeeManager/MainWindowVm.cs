﻿using EmployeeManager.Controls;
using EmployeeManager.DAO;
using EmployeeManager.Models;
using EmployeeManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace EmployeeManager
{
    public class MainWindowVm : INotifyPropertyChanged
    {
        private readonly IRepository repository; 
        public MainWindowVm(IRepository repository, SaveItem saveItem)
        {
            this.repository = repository;
            Initialize();
            SaveItemContent = saveItem;
        }

        public UserControl SaveItemContent { get; }

        private RelayCommand newButtonExecute;
        public RelayCommand NewButtonExecute
        {
            get
            {
                return newButtonExecute ??
                  (newButtonExecute = new RelayCommand(obj =>
                  {
                      ((SaveItemVm)SaveItemContent.DataContext).EmpModel = new EmplDocPhoneModel();
                      ((SaveItemVm)SaveItemContent.DataContext).IsVisible = Visibility.Visible;
                  }));
            }
        }

        private RelayCommand changeButtonExecute;
        public RelayCommand ChangeButtonExecute
        {
            get
            {
                return changeButtonExecute ??
                  (changeButtonExecute = new RelayCommand(obj =>
                  {
                      ((SaveItemVm)SaveItemContent.DataContext).EmpModel = MainTableSelected;
                      ((SaveItemVm)SaveItemContent.DataContext).IsVisible = Visibility.Visible;
                  }));
            }
        }

        private RelayCommand refreshButtonExecute;
        public RelayCommand RefreshButtonExecute
        {
            get
            {
                return refreshButtonExecute ??
                  (refreshButtonExecute = new RelayCommand(async obj =>
                  {
                      MainTable = new ObservableCollection<EmplDocPhoneModel>(await repository.GetEmplDocPhone());
                  }));
            }
        }

        private RelayCommand deleteButtonExecute;
        public RelayCommand DeleteButtonExecute
        {
            get
            {
                return deleteButtonExecute ??
                  (deleteButtonExecute = new RelayCommand(async obj =>
                  {
                      try
                      {
                          await repository.DeleteEmplDocPhone(MainTableSelected);
                          MessageBox.Show("Данные успешно удалены");
                      }
                      catch(Exception e)
                      {
                          MessageBox.Show(e.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                      }
                  }));
            }
        }

        private ObservableCollection<EmplDocPhoneModel> mainTable;
        public ObservableCollection<EmplDocPhoneModel> MainTable 
        {
            get
            {
                return mainTable;
            }
            set
            {
                mainTable = value;
                OnPropertyChanged(nameof(MainTable));
            }
        }

        private EmplDocPhoneModel mainTableSelected;
        public EmplDocPhoneModel MainTableSelected
        {
            get
            {
                return mainTableSelected;
            }
            set
            {
                mainTableSelected = value;
                OnPropertyChanged(nameof(MainTableSelected));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public async void Initialize()
        {
            try
            {
                MainTable = new ObservableCollection<EmplDocPhoneModel>(await repository.GetEmplDocPhone());
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
