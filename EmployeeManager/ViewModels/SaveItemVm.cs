﻿using EmployeeManager.DAO;
using EmployeeManager.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EmployeeManager.ViewModels
{
    public class SaveItemVm : INotifyPropertyChanged
    {
        private IRepository repo;

        public SaveItemVm(IRepository repo)
        {
            this.repo = repo;
            IsVisible = Visibility.Collapsed;
        }

        private Visibility isVisible;
        public Visibility IsVisible
        {
            get
            {
                return isVisible;
            }
            set
            {
                isVisible = value;
                OnPropertyChanged(nameof(IsVisible));
            }
        }
        private EmplDocPhoneModel empModel;
        public EmplDocPhoneModel EmpModel
        {
            get
            {
                return empModel;
            }
            set
            {
                empModel = value;
                OnPropertyChanged(nameof(EmpModel));
            }
        }

        private RelayCommand okCommand;
        public RelayCommand OkCommand
        {
            get
            {
                return okCommand ??
                  (okCommand = new RelayCommand(obj =>
                  {
                      OkCommandExecute();
                  }));
            }
        }
        private async void OkCommandExecute()
        {
            if (IsDateValid(EmpModel.BirthDtm))
            {
                try
                {
                    if (EmpModel.EmplId == 0)
                    {
                        await repo.AddEmplDocPhone(EmpModel);
                    }
                    else
                    {
                        await repo.UpdateEmplDocPhone(EmpModel);
                    }
                    MessageBox.Show("Данные успешно обновлены");
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Возраст не может быть меньше 18 или больше 100 лет", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private RelayCommand cancelCommand;
        public RelayCommand CancelCommand
        {
            get
            {
                return cancelCommand ??
                  (cancelCommand = new RelayCommand(obj =>
                  {
                      CancelCommandExecute();
                  }));
            }
        }
        private void CancelCommandExecute()
        {
            IsVisible = Visibility.Collapsed;
        }
      
        private bool IsDateValid(DateTime birthDate)
        {
            var result = true;
            var age = DateTime.Now.Year - birthDate.Year;
            if(age < 18 || age > 100){
                result = false;
            }

            return result;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
