﻿using Autofac;
using EmployeeManager.Controls;
using EmployeeManager.DAO;
using EmployeeManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManager.Infrastructure
{
    public static class DiContainer
    {
        public static IContainer Container;
        static DiContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Repository>().As<IRepository>();

            builder.RegisterType<SaveItem>();

            builder.RegisterType<MainWindowVm>();
            builder.RegisterType<SaveItemVm>();

            Container = builder.Build();
        }
    }
}
